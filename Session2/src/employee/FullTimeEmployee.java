package employee;

public class FullTimeEmployee extends Employee {

	public FullTimeEmployee(String name, int paymentPerHour) {
		super(name, paymentPerHour);//similar to this() but calling the parent constructor
	}

	@Override
	public int calculateSalary() {
        return getPaymentPerHour() * 8;

	}

}
