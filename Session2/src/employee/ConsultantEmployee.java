package employee;

public class ConsultantEmployee extends Employee {

	public ConsultantEmployee(String name, int paymentPerHour) {
		super(name, paymentPerHour);
	}

	@Override
	public int calculateSalary() {
		return getPaymentPerHour() * 2 * 8;
	}

}
