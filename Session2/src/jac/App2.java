package jac;

class Student{
	String name;
	static String collegeName = "JAC";
}


class Machine{
	static int counterOfCreation = 0; //class variable
	String name; //instance variable
	
	//default constructor => it doesn't return anything and name is the same as class
	public Machine() {
		counterOfCreation++;
	}
	
	public void run() {
		System.out.println("I am a machine my name is " + this.name);
	}
	
	public static void commonRun() {
		//factory(); static methods cannot call NON-static method
		factoryCommon(); // it can be called because the method is static
		System.out.println("WE love Java " + counterOfCreation);
	}
	
	public void factory() {
		//it does nothing
	}
	public static void factoryCommon() {
		//it does nothing
	}
}


public class App2 {

	
	//Static fields
	public static void main(String[] args) {	
		Student st1 = new Student();
		st1.name = "Reza";
		
		Student st2 = new Student();
		st2.name = "Betty";
		
		System.out.println(st1.name+ "  " + st1.collegeName);
		System.out.println(st2.name+ "  " + st2.collegeName);
		System.out.println(Student.collegeName);//static fields are a part of the class

		
		//I would like to see how many machine I am creating!!!??
		//think about the constructors
		
		Machine machine1 = new Machine();
		machine1.name = "A";
		//System.out.println(machine1.counterOfCreation);
		Machine machine2 = new Machine();
		machine2.name = "B";
		System.out.println(Machine.counterOfCreation);
		Machine machine3 = new Machine();
		machine3.name = "C";
		//System.out.println(machine3.counterOfCreation);
		Machine machine4 = new Machine();
		machine4.name = "D";
			
		System.out.println(Machine.counterOfCreation);
		
		Student.collegeName = "Dawson";
		System.out.println(Student.collegeName);
		
		
		machine1.run();
		machine2.run();
		machine3.run();
		machine4.run();
		
		Machine.commonRun();
		
		
	}

}
