package jac;

class Person{
	String name;
	
	public Person(String name) {
		this.name= name;
	}
}

class Temp{
	int num;
}

public class App5 {
	public static void main(String[] args) {
		//Call by value and call by reference
		
		//Send a primitive to a method
		// int, short, double....
		
		int num = 5;
		System.out.println("the value of num before calling the method = " + num);
		changeValue(num);
		//since Java is call by value,so the value won't change
		System.out.println("the value of num after calling the method = " + num);
	
		//send a reference to a method
		Person p = new Person("TOTO");
		System.out.println("name of the person before calling the method is = " + p.name);
		changeName(p);
		System.out.println("name of the person after calling the method is = " + p.name);
	}
	
	//primitive
	public static void changeValue(int num) {
		// it uses STACK to keep the value
		num *= 2;
	}
	
	//reference
	public static void changeValue(Temp p) {
		p.num *=2;
	}
	
	public static void changeName(Person p) {
		p.name = "POPO"; 
	}
}
