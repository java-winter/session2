package jac;

class Account{
	private int id;
	private double balance;
	
	
	public Account(int id, double balance) {
		if (id >0) {
			this.id = id;
			setBalance(balance);
		}
		else {
			System.err.println("Error: the value is not valid");

		}
	}
	
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		if (balance >=0 && balance <=100000) {
			this.balance = balance;
		}
		else {
			System.out.println("WARNING: the value is not valid");
		}
			
		
	}
	public int getId() {
		return id;
	}
	
}

public class App3 {
	
	public static void main(String[] args) {
		Account account1  = new Account(1, 20);
		account1.setBalance(-2);
		
		//ctrl + 2 ... l
		double balance = account1.getBalance();
		
		
		Account account2 = new Account(-1, 20);

	}
}
