package jac;

//private and public

class A{
	private int x; //it is only accessible within the class
	public int y;//EVERY CLASSES can have access to that
	
	private void methodA() {
		x = 10;
	}
	
	private void caller() {
		methodA(); // is private but ACCESSIBLE within the same class
		x = 11;
		y = 5;
	}
}


class B{
	public void methodA() {
		A a= new A();
		//a.methodA(); compiler error calling a private method is not possible in ANOTHER class
		//a.x; compiler error because private member is NOT accisible outside of the class
		a.y = 50;// y is public so it is accessible everywhere 
	}
}

public class App4 {
	
	public static void main(String[] args) {
		
	}
}
