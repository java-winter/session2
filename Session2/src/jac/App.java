package jac;


class UserManagement{
	
	//when you define a method private you won't have access to
	//that method
	
	public void manageUsers() {
		User user1 = new User();//it is not a good practice !!!
		user1.setUserEmail("a@gmail.com");
		user1.setUserId(1);
		user1.setUserName("toto");
		
		User user2 = new User(2, "Popo", "b@gmail.com");
		
	}
}

class User{
	private int userId;
	private String userName;
	private String userEmail;
	
	//the constructors help us to initialize an object with the desired values
	public User(int userId, String userName, String userEmail) {
		//super();//I will talk about that in inheritance
		this.userId = userId;
		this.userName = userName;
		this.userEmail = userEmail;
		
		// OR
		
//		setUserName(userName);
//		setUserId(userId);
//		setUserEmail(userEmail);
		
	}
	
	
	//default constructor
	public User() {
		
	}



	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	
	
	
}

public class App {

	public static void main(String[] args) {
		UserManagement manage = new UserManagement();
		manage.manageUsers();
	}

}
