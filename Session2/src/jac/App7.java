package jac;

class Chef{
	int experiance;
	boolean mustache;
	
	public Chef(int experiance) {
		this(experiance, true);
	}

	public Chef(int experiance, boolean mustache) {
		this.experiance = experiance;
		this.mustache = mustache;
	}
	
	
}

class Teacher{
	int age;
	String name;
	
	public Teacher(String name, int age) {
		this.age = age;
		this.name = name;
	}
	
	public Teacher() {
		this("", 0);
	}
	
	private void sayHi() {
		this.age = 12; //calling a variable within the same class
		System.out.println("Hello" + this.name);
	}
	
	public void sayBye() {
		//this(); you can only call the constructor in another constructor
		
		//you can add some validations to check if you are eligible to call the private method
		this.sayHi();//whenever we call a method within the same class
		
		
	}
}

public class App7 {
	public static void main(String[] args) {
		
		//layers
		//packages like service, DAL, UI
		
		Teacher t = new Teacher("A", 30);
		t.sayBye();
	}
}
