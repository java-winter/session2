package jac;

import java.lang.*;

class PartTimeEmployee{
	final int hourlyWork = 25;
	final static String companyName = "Google";
	
	public double calculateSalary(int hourlyRate) {
		
		//hourlyWork += 5; compile error if you want to change it
	
		return hourlyRate * hourlyWork;
	}
}


public class App6 {
	public static void main(String[] args) {
		String name= "my name ";//name is immutable (string)
		String changedStr = name.concat(" is reza");
		
		String name2 = new String();
		String name3 = new String("Alex");
		System.out.println(name3);
		
		//SELF study about sting built-in methods
		
		System.out.println(changedStr);
		
		
		//PartTimeEmployee.companyName = "Apple"; compile error because final cannnot be changed
	}
}
